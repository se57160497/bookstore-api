package com.jadtawat.bookstore.api;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookstoreRepository extends MongoRepository<Bookstore,Integer> {

}
