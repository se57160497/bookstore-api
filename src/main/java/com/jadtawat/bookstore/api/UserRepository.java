package com.jadtawat.bookstore.api;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User,String> {
	List<User> findByUsernameAndPassword(String username, String password);

	List<User> findByUsername(String username);
}
