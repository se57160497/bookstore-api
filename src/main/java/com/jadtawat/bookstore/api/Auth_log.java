package com.jadtawat.bookstore.api;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "auth_log")
public class Auth_log {
	@Id
	private String id;
	
	private Date createdAt;
	
	private String session;
	
	private String objid;

	public Auth_log(String id, Date createdAt, String session, String objid) {
		super();
		this.id = id;
		this.createdAt = createdAt;
		this.session = session;
		this.objid = objid;
	}
	
	public Auth_log() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public String getObjid() {
		return objid;
	}

	public void setObjid(String objid) {
		this.objid = objid;
	}
	

}
