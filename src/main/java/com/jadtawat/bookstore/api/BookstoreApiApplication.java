package com.jadtawat.bookstore.api;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.Reader;
//import java.io.UnsupportedEncodingException;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.expression.spel.ast.TypeReference;

//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.ObjectMapper;



@SpringBootApplication
public class BookstoreApiApplication {
	
//	class EmployeList {
//	    private List<User> compemployes;
//
//		public List<User> getCompemployes() {
//			return compemployes;
//		}
//
//		public void setCompemployes(List<User> compemployes) {
//			this.compemployes = compemployes;
//		}
//	}

	public static void main(String[] args) {
//		String book = CurlMapping("https://scb-test-book-publisher.herokuapp.com/books");
//		ObjectMapper mapper = new ObjectMapper();
//		Bookstore[] books = mapper.readValue(book, Bookstore[].class);
//		System.out.println(books);

		SpringApplication.run(BookstoreApiApplication.class, args);
	}
	
//	public static String CurlMapping(String URL) throws MalformedURLException {
//		URL url = new URL(URL);
//		String rtn = "";
//		try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"))) {
//		    for (String line; (line = reader.readLine()) != null;) {
//		        rtn += line;
//		    }
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "";
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return "";
//		}
//		return rtn;
//	}

}
