package com.jadtawat.bookstore.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.jadtawat.bookstore.utils.PasswordUtil;

class ResponseBooks {
	public List<Bookstore> books;

	ResponseBooks(List<Bookstore> setBooks) {
		this.books = setBooks;
	}
}

//
class ReponseLogin {
	public String name;
	public String surname;
	public String date_of_birth;
	public ArrayList<String> books;

	ReponseLogin(User setUser) {
		this.name = setUser.getName();
		this.surname = setUser.getSurname();
		this.date_of_birth = setUser.getdate_of_birth();
		this.books = setUser.getBooks();
	}
}

class ResponseError {
	public String message;

	public ResponseError(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}

class QueryLogin {
	public String username;
	public String password;

	public QueryLogin(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}


@RestController
public class BookstoreController {

	@Autowired
	private BookstoreRepository books_store;

	@Autowired
	private UserRepository user_repo;

	@Autowired
	private Auth_logRepository auth_repo;

//	@RequestMapping("/books") 
//	public ResponseBooks getAllBookstores() {
//		List<Bookstore> bookstores = books_store.findAll();
//		ResponseBooks books = new ResponseBooks(bookstores);
//	    return books;
//	}

	@RequestMapping(path = "/login", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> Login(@RequestBody User loginForm, @Context HttpServletRequest request) {
		String session = request.getSession().getId();
		List<User> us = user_repo.findByUsername(loginForm.getUsername());
		if (!us.isEmpty() && PasswordUtil.verifyUserPassword(loginForm.getPassword(), us.get(0).getPassword(),
				us.get(0).getSalt())) {

			List<Auth_log> auth = auth_repo.findBysession(session);
			if (!auth.isEmpty()) {
				auth_repo.deleteById(auth.get(0).getId());
			}

			Auth_log auth_insert = new Auth_log();
			auth_insert.setSession(session);
			auth_insert.setObjid(us.get(0).getId());
			auth_insert.setCreatedAt(new Date());
			auth_repo.insert(auth_insert);

			return ResponseEntity.ok(new ReponseLogin(us.get(0)));
		} else {
			return ResponseEntity.ok(new ResponseError("Have wrong Username or Password, Please try again."));
		}
	}

	@RequestMapping(path = "/users", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> UserDetail(@Context HttpServletRequest request) {
		String session = request.getSession().getId();
		List<Auth_log> auth = auth_repo.findBysession(session);
		if (!auth.isEmpty()) {
			Optional<User> opt_us = user_repo.findById(auth.get(0).getObjid());
			if (opt_us.isPresent()) {
				User us = opt_us.get();
				return ResponseEntity.ok(new ReponseLogin(us));
			} else {
				return ResponseEntity.ok(new ResponseError("Cannot get user detail, Please try again."));
			}
		} else {
			return ResponseEntity.ok(new ResponseError("Please login."));
		}
	}

	@RequestMapping(path = "/books", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> GetBook(@Context HttpServletRequest request) {
		ArrayList<Bookstore> books = getBooks();
		if(books.isEmpty()) {
			return ResponseEntity.ok(new ResponseError("Books out of stock"));
		}
		return ResponseEntity.ok(books);
	}

	@RequestMapping(path = "/users", method = RequestMethod.DELETE, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> UserDelete(@Context HttpServletRequest request) {
		String session = request.getSession().getId();
		List<Auth_log> auth = auth_repo.findBysession(session);
		if (!auth.isEmpty()) {
			Optional<User> opt_us = user_repo.findById(auth.get(0).getObjid());
			if (opt_us.isPresent()) {
				User us = opt_us.get();
				user_repo.deleteById(us.getId());
				for(int i = 0 ;i<auth.size();i++) {
					auth_repo.deleteById(auth.get(i).getId());
				}
				List<Auth_log> auth_user = auth_repo.findByobjid(us.getId());
				for(int i = 0 ;i<auth_user.size();i++) {
					auth_repo.deleteById(auth_user.get(i).getId());
				}
				return ResponseEntity.ok(new ReponseLogin(us));
			} else {
				return ResponseEntity.ok(new ResponseError("Cannot get user detail, Please try again."));
			}
		} else {
			return ResponseEntity.ok(new ResponseError("Please login."));
		}
	}

	@RequestMapping(path = "/users", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> CreateUser(@RequestBody User loginForm) {
		String salt = PasswordUtil.getSalt(30);
		loginForm.setSalt(salt);
		String mySecurePassword = PasswordUtil.generateSecurePassword(loginForm.getPassword(), salt);
		List<User> us = user_repo.findByUsername(loginForm.getUsername());
		if (!us.isEmpty()) {
			return ResponseEntity.ok(new ResponseError("User Duplicated"));
		} else {
			loginForm.setPassword(mySecurePassword);
			System.out.print(user_repo.insert(loginForm));
			return ResponseEntity.ok(new ResponseError("Registration Success"));
		}
	}
	
	@RequestMapping(path = "/users/orders", method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<?> OrderBooks(@RequestBody Orderbook orders, @Context HttpServletRequest request) {
		List<Bookstore> books = getBooks();
		if(books.isEmpty()) {
			return ResponseEntity.ok(new ResponseError("Books out of stock"));
		}
		ArrayList<String> book_id = new ArrayList<String>();
		Integer[] order = orders.getOrders();
		float price = 0;
		int count_book =0;
		for(int i = 0;i < order.length;i++) {
			int id = order[i];
			for(int j = 0;j < books.size();j++) {
				Bookstore book = books.get(j);
				if (book.getId() == id) {
					price += book.getPrice();
					book_id.add(book.getId().toString());
					count_book++;
				}
			}
		}
		if(count_book < order.length) {
			return ResponseEntity.ok(new ResponseError("Book Not Found"));
		}
		System.out.println(count_book+" : "+order.length);
		String session = request.getSession().getId();
		List<Auth_log> auth = auth_repo.findBysession(session);
		if (!auth.isEmpty()) {
			Optional<User> opt_us = user_repo.findById(auth.get(0).getObjid());
			if(opt_us.isPresent()) {
				User toSave = opt_us.get();
				ArrayList<String> book = toSave.getBooks();
				book.addAll(book_id);
				toSave.setBooks(book);
				user_repo.save(toSave);
				return ResponseEntity.ok(price);
			}else {
				return ResponseEntity.ok(new ResponseError("User Not Found"));
			}
		}else {
			return ResponseEntity.ok(new ResponseError("Please Login"));
		}
	}
	

	private ArrayList<Bookstore> getBooks() {
		ArrayList<Bookstore> book_normal = getBooksNormal();
		ArrayList<Bookstore> book_recom = getBooksRecom();
		ArrayList<Bookstore> book_rtn = new ArrayList<Bookstore>();
//		book_recom.addAll(book_normal);
		if(book_recom.isEmpty() && book_recom.isEmpty()) {
			return book_recom;
		}
//		Collections.sort(book_recom, new Comparator<Bookstore>() {
//			@Override
//			public int compare(Bookstore o1, Bookstore o2) {
//				return o1.getId().compareTo(o2.getId());
//			}
//		});
		book_rtn = checkDuplicate(book_recom,book_rtn);
		book_rtn = checkDuplicate(book_normal,book_rtn);

		return book_rtn;
	}
	private ArrayList<Bookstore> checkDuplicate(ArrayList<Bookstore>book, ArrayList<Bookstore>book_rtn) {
		for (int i = 0; i < book.size(); i++) {
			if (i == 0) {
				book_rtn.add(book.get(i));
			} else {
				boolean check_dup = false;
				for (int j = 0; j < book_rtn.size(); j++) {
					if (book.get(i).getId() == book_rtn.get(j).getId()) {
						check_dup = true;
					}
				}
				if (!check_dup) {
					book_rtn.add(book.get(i));
				}
			}
		}
		return book_rtn;
	}

	private ArrayList<Bookstore> getBooksRecom() {
		return getBookByJson(getJson("https://scb-test-book-publisher.herokuapp.com/books/recommendation"));
	}

	private ArrayList<Bookstore> getBooksNormal() {
		return getBookByJson(getJson("https://scb-test-book-publisher.herokuapp.com/books"));
	}

	private JSONArray getJson(String url) {
		RestClient rest = new RestClient();
		String str_json = "{data : " + rest.get(url) + " }";
		JSONObject mainObj = new JSONObject(str_json);
		return (JSONArray) mainObj.getJSONArray("data");
	}

	private ArrayList<Bookstore> getBookByJson(JSONArray jsonArray) {
		ArrayList<Bookstore> book = new ArrayList<Bookstore>();
		for (int i = 0; i < jsonArray.length(); i++) {
			System.out.println();
			JSONObject obj = jsonArray.getJSONObject(i);
			String author_name = obj.getString("author_name");
			Float price = obj.getFloat("price");
			Integer id = obj.getInt("id");
			String book_name = obj.getString("book_name");
			book.add(new Bookstore(id, book_name, author_name, price));
		}
		return book;
	}

}
