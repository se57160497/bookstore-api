package com.jadtawat.bookstore.api;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "bookstore_recomen")
public class Bookstore_recomen {
	@Id
	private String _id;
	private String id;
    private String book_name;
	private String author_name;
    private float    price;
    
	public Bookstore_recomen(String _id,String id, String book_name, String author_name, float price) {
		this._id = _id;
		this.id = id;
		this.book_name = book_name;
		this.author_name = author_name;
		this.price = price;
	}
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getbook_name() {
		return book_name;
	}
	public void setbook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getauthor_name() {
		return author_name;
	}
	public void setauthor_name(String author_name) {
		this.author_name = author_name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}

