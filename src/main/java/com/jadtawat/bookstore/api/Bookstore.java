package com.jadtawat.bookstore.api;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Document(collection = "bookstore")
public class Bookstore {
	@Id
	private Integer id;
    private String book_name;
	private String author_name;
    private float    price;
    private boolean recommend;
    

	
	public Bookstore(Integer id, String book_name, String author_name, float price) {
		this.id = id;
		this.book_name = book_name;
		this.author_name = author_name;
		this.price = price;
	}

	public String getbook_name() {
		return book_name;
	}
	public void setbook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getauthor_name() {
		return author_name;
	}
	public void setauthor_name(String author_name) {
		this.author_name = author_name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public boolean isRecommend() {
		return recommend;
	}

	public void setRecommend(boolean recommend) {
		this.recommend = recommend;
	}
	

}
