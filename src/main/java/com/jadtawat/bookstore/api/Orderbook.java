package com.jadtawat.bookstore.api;


import java.util.ArrayList;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "orderbook")
public class Orderbook {

	private Integer[] orders;
//	ArrayList<String>

	public Orderbook(Integer[] orders){
	
	        this.orders = orders;
	    }

	public Orderbook() {
		// TODO Auto-generated constructor stub
	}

	public Integer[] getOrders() {
		return orders;
	}

	public void setOrders(Integer[] orders) {
		this.orders = orders;
	}

}


