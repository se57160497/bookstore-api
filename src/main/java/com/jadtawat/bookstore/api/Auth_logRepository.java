package com.jadtawat.bookstore.api;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface Auth_logRepository extends MongoRepository<Auth_log,String> {
	
	List<Auth_log> findByobjid(String objid);
	
	List<Auth_log> findBysession(String session);
	
}
